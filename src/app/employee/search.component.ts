import { Component, OnInit } from '@angular/core';
import { EmployeeService } from './employee.service';

@Component({
  selector: 'employee-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit{
    employees: any[];

    constructor(private employeeService: EmployeeService) { }

    ngOnInit(): void {
        this.employeeService.getEmployees()
            .subscribe((data : any[]) => {
                this.employees = data;
                console.log(data);
            });
    }
}
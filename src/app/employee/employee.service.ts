import { Injectable } from '@angular/core';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { Observable } from '../../../node_modules/rxjs/Observable';

@Injectable()
export class EmployeeService {

  constructor(private httpClient: HttpClient) { }

  apiUrl = "http://localhost:8082/api/";

  getEmployees(){
      return this.httpClient.get(this.apiUrl + "employees");
  }
}